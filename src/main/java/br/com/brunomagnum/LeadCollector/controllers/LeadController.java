package br.com.brunomagnum.LeadCollector.controllers;

import br.com.brunomagnum.LeadCollector.enums.TipoLeadEnum;
import br.com.brunomagnum.LeadCollector.models.Lead;
import br.com.brunomagnum.LeadCollector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    public Lead registrarLead(@RequestBody Lead lead){

        return leadService.salvarLead(lead);
    }

    @GetMapping
    public Iterable<Lead> exibirTodos(@RequestParam(name= "tipoDeLead",required = false)TipoLeadEnum tipoLeadEnum){
        if (tipoLeadEnum != null) {
            return leadService.buscarTodosPorTipoLead(tipoLeadEnum);
        }
        return leadService.buscarTodos();
    }

    @GetMapping("/{id}")
    public Lead buscarPorId(@PathVariable(name= "id") int id) {
        try {
            return leadService.buscarPorId(id);
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable(name= "id") int id, @RequestBody Lead lead) {
        try {
            return leadService.atualizarLead(id, lead);
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarLead(@PathVariable int id){
        try{
            leadService.deletarLead(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
