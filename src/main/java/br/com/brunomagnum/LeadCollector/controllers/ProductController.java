package br.com.brunomagnum.LeadCollector.controllers;

import br.com.brunomagnum.LeadCollector.models.Product;
import br.com.brunomagnum.LeadCollector.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.Id;

@RestController
@RequestMapping("/Products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public Iterable<Product> exibirTodos(Integer id){
        return productService.buscarTodos();
    }

    @PostMapping
    public Product registerProduct(@RequestBody Product product){

        return productService.salvarProduct(product);
    }

    @GetMapping("/{id}")
    public Product buscarPorId(@PathVariable(name= "id")int id){
        try{
            return productService.buscarPorId(id);
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Product atualizarProduct(@PathVariable(name="id") int id, @RequestBody Product product){
        try{
            return productService.atualizarProduct(id, product);
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduct(@PathVariable(name= "id") int id){
        try{
            productService.deletarProduct(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
