package br.com.brunomagnum.LeadCollector.services;

import br.com.brunomagnum.LeadCollector.models.Product;
import br.com.brunomagnum.LeadCollector.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Iterable<Product> buscarTodos(){
        return productRepository.findAll();
    }

    public Product salvarProduct(Product product) {
        return productRepository.save(product);
    }

    public Product buscarPorId(int id){
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()){
            return optionalProduct.get();
        }
        throw new RuntimeException("Product não encontrado");
    }

    public Product atualizarProduct(int id, Product product){
        if (productRepository.existsById(id)){
            product.setId(id);

            return salvarProduct(product);
        }
        throw new RuntimeException("Product nõ encontrado");
    }

    public ResponseEntity deletarProduct(int id){
        if (productRepository.existsById(id)){
            productRepository.deleteById(id);

        }
        throw new RuntimeException("Product não encontrado");
    }


}
