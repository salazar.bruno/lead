package br.com.brunomagnum.LeadCollector.services;

import br.com.brunomagnum.LeadCollector.enums.TipoLeadEnum;
import br.com.brunomagnum.LeadCollector.models.Lead;
import br.com.brunomagnum.LeadCollector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    public Iterable<Lead> buscarTodos(){
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;
    }

    public Lead salvarLead(Lead lead) {
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodosPorTipoLead(TipoLeadEnum leadEnum){
        Iterable<Lead> leads = leadRepository.findAllByTipoLead(leadEnum);
        return leads;
    }

    public Lead buscarPorId(int id){
        Optional<Lead> optionalLead = leadRepository.findById(id);
        if (optionalLead.isPresent()){
                return optionalLead.get();
        }
        throw new RuntimeException("Lead Não Encontrado");
    }

    public Lead atualizarLead(int id, Lead lead){
        if (leadRepository.existsById(id)){
            lead.setId(id);

            return salvarLead(lead);
        }
        throw new RuntimeException("Lead Não Encontrado");
    }

    public Void deletarLead(int id) {
        if (leadRepository.existsById(id)){
            leadRepository.deleteById(id);
        }
        throw new RuntimeException("Lead Não Encontrado");
    }
}
