package br.com.brunomagnum.LeadCollector.repositories;

import br.com.brunomagnum.LeadCollector.enums.TipoLeadEnum;
import br.com.brunomagnum.LeadCollector.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {
    Iterable<Lead> findAllByTipoLead(TipoLeadEnum tipoLeadEnum);
}
