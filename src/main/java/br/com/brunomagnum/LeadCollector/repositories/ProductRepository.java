package br.com.brunomagnum.LeadCollector.repositories;

import br.com.brunomagnum.LeadCollector.models.Lead;
import br.com.brunomagnum.LeadCollector.models.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
